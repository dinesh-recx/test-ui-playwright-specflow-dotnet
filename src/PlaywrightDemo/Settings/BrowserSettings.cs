﻿
namespace Practice.One.UI.Settings.Configuration
{
    public class BrowserSettings
    {
        public int PageLoadTimeout { get; set; } = 3000;
        public int ScriptTimeout { get; set; } = 1000;
        public int ArtificialDelayBeforeAction { get; set; } = 0;
        public int ArtificialDelayAfterAction { get; set; } = 0;
        public int ArtificialDelayBeforeNavigation { get; set; } = 0;
        public int ArtificialDelayAfterNavigation { get; set; } = 0;
        public int ArtificialDelayBeforeRefresh { get; set; } = 0;
        public int ArtificialDelayAfterRefresh { get; set; } = 0;
        public int ArtificialDelayBeforeWindow  { get; set; } = 0;
        public int ArtificialDelayAfterWindow  { get; set; } = 0;
    }
}
