namespace PlaywrightDemo.Pages.Sections
{
    public class FooterSection
    {

    }
    public class FooterSectionModel
    {
        public string FooterText { get; set; }
        public string FooterLink { get; set; }
        public string FooterLinkText { get; set; }
        public string FooterLinkTarget { get; set; }
    }
}